# # Library and data import

import pandas as pd

import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

import seaborn as sns
from sklearn.metrics import silhouette_score, davies_bouldin_score
from sklearn.neighbors import NearestNeighbors
from kneed import KneeLocator
from sklearn.cluster import AgglomerativeClustering, KMeans, DBSCAN
from scipy.cluster.hierarchy import dendrogram, linkage

import umap
import umap.plot
from sklearn.preprocessing import StandardScaler

# Read in color matrix

# Pfade zur CSV-Datei: Color matrix without greyscale
csv_file_path = r"./Data/mapped_Matrix_addition_ohne_unbunt.csv"

# CSV-Datei lesen
df = pd.read_csv(csv_file_path, index_col=0, encoding="utf-8" )

# Read in USK data

game_data = pd.read_csv(r"./Data/gamelist_extended_final_new.csv", encoding= "utf-8", delimiter= ";")
getting_usk = game_data[["Trailer_short", "Age"]]
getting_usk=getting_usk.rename(columns={'Age': 'USK'})
getting_usk.set_index("Trailer_short", inplace=True)
getting_usk

data = df

# UMAP

reducer = umap.UMAP(min_dist=0.01, metric= "correlation", n_jobs=1, random_state=1)
scaled_data = StandardScaler().fit_transform(data)
embedding = reducer.fit_transform(scaled_data)
#print(embedding)
embedding = pd.DataFrame(embedding)
embedding.rename(columns={0: 'umap 1', 1: 'umap 2'}, inplace=True)
embedding.index = df.index
embedding
plt.scatter(embedding.iloc[:,0], embedding.iloc[:,1])
plt.title("UMAP")
plt.xlabel("umap 1")
plt.ylabel("umap 2")
plt.savefig("./Output/plot_umap.svg",bbox_inches='tight')

cluster_data = embedding.copy()
cluster_data

# Join data sets
embedding = embedding.join(getting_usk).drop_duplicates()

# # K-Means

# Elbow method to determine best k

inertias = []

for i in range(2, 30):
    kmeans = KMeans(n_clusters=i, random_state=0, max_iter= 10000)
    kmeans.fit(cluster_data)
    inertias.append(kmeans.inertia_)

plt.plot(range(2,30), inertias, marker='o')
plt.title('Elbow method')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.savefig("./Output/kmeans_elbow.svg",bbox_inches='tight')

# Find best number of clusters

# List to store silhouette scores for different cluster numbers
silhouette_scores = []

# Try different numbers of clusters
for n_clusters in range(2, 30):
    clustering = KMeans(n_clusters=n_clusters, random_state=0, max_iter=300, init="k-means++", n_init="auto")
    kmeans_labels =  clustering.fit_predict(cluster_data)
    silhouette_avg = silhouette_score(cluster_data, kmeans_labels)
    silhouette_scores.append((n_clusters, silhouette_avg))

# Print silhouette scores for different cluster numbers
for n_clusters, silhouette_avg in silhouette_scores:
    print(f"For n_clusters = {n_clusters}, the average silhouette_score is: {silhouette_avg}")

# Find the best number of clusters based on the highest silhouette score
best_n_clusters = max(silhouette_scores, key=lambda x: x[1])[0]
print(f"Best number of clusters: {best_n_clusters}")


# Plot K-means

kmeans = KMeans(n_clusters = best_n_clusters, init= "k-means++", random_state= 0, n_init="auto", max_iter= 300, tol=0.0001).fit(cluster_data)
labels_kmeans = kmeans.labels_
inertia = kmeans.inertia_
centers = kmeans.cluster_centers_

# Add K-mans labels to data frame
embedding["kmeans labels"] = labels_kmeans

u_labels = np.unique(labels_kmeans)
 
# Plotting the results:
cmap = plt.get_cmap("tab20")
for i, label in enumerate(u_labels):
    color = cmap((i)/len(u_labels))
    plt.scatter(cluster_data.iloc[labels_kmeans == label , 0] , 
                cluster_data.iloc[labels_kmeans== label , 1] , 
                label = label, color = color )
    plt.scatter(centers[:,0] , centers[:,1] , s = 10, color = 'k')
plt.legend(loc="center left", bbox_to_anchor=(1, 0.5), title="Clusters")
plt.title("K-Means")
plt.xlabel("umap 1")
plt.ylabel("umap 2")
plt.savefig("./Output/plot_kmeans.svg",bbox_inches='tight')
plt.show()

# K-means scores
sil_kmeans = silhouette_score(cluster_data, labels_kmeans)
davies_kmeans = davies_bouldin = davies_bouldin_score(cluster_data, labels_kmeans)
print("davies_bouldin_score", davies_kmeans)
print("silhouette_score", sil_kmeans)

# # DBSCAN

# Find neigh_distance

nbrs = NearestNeighbors(n_neighbors = 4).fit(cluster_data)

# Find the k-neighbors of a point
neigh_distance, neigh_indices = nbrs.kneighbors(cluster_data)

# Sort the neighbor distances (lengths to points) in ascending order
# The axis = 0 represents sort along first axis i.e. sort along row
sort_neigh_distance = np.sort(neigh_distance, axis= 0 ) #axis = 0
k_dist = sort_neigh_distance[:,1]
plt.plot(k_dist)
plt.ylabel("k-NN distance")
plt.xlabel("Sorted observations (2nd NN)")

plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.savefig("./Output/dbscan_kNN.svg",bbox_inches='tight')
plt.show()

# Find best eps for DBSCAN
kneedle = KneeLocator(x = range(1, len(neigh_distance)+1), y = k_dist, S = 1.0, 
                      curve = "convex", direction = "increasing")
# Get the estimate of knee point
print(kneedle.knee_y)
best_eps = kneedle.knee_y

# Plot DBSCAN
dbscan = DBSCAN(eps= best_eps, min_samples =4 ).fit(cluster_data)
labels_DBSCAN = dbscan.labels_

# Add DBSCAN labels to dataframe
embedding.loc[:,"DBSCAN_labels"] = labels_DBSCAN

# Remove label -1 (noise)
u_labels = np.unique(labels_DBSCAN)[1:]
 
# Plot results
for i, label in enumerate(u_labels):
    color = cmap((i)/len(u_labels))
    plt.scatter(cluster_data.iloc[labels_DBSCAN == label , 0] , 
                cluster_data.iloc[labels_DBSCAN == label , 1] , 
                label = label, color = color )
plt.legend(loc="center left", bbox_to_anchor=(1, 0.5),title="Clusters")
plt.title("DBSCAN")
plt.xlabel("umap 1")
plt.ylabel("umap 2")
plt.savefig("./Output/plot_dbscan.svg",bbox_inches='tight')
plt.show()

# Scores for DBSCAN

# DBSCAN scores
sil_dbscan = silhouette_score(cluster_data, labels_DBSCAN)
davies_dbscan = davies_bouldin = davies_bouldin_score(cluster_data, labels_DBSCAN)
print("davies_bouldin_score", davies_dbscan)
print("silhouette_score",sil_dbscan)

# # Hierarchical Clustering

# Find best number of clusters

# List to store silhouette scores for different cluster numbers
silhouette_scores = []

# Try different numbers of clusters
for n_clusters in range(2, 30):
    clustering =  AgglomerativeClustering(n_clusters= n_clusters, metric="euclidean",linkage="ward")
    hierarchical_labels =  clustering.fit_predict(cluster_data)
    silhouette_avg = silhouette_score(cluster_data, hierarchical_labels)
    silhouette_scores.append((n_clusters, silhouette_avg))

# Print silhouette scores for different cluster numbers
for n_clusters, silhouette_avg in silhouette_scores:
    print(f"For n_clusters = {n_clusters}, the average silhouette_score is: {silhouette_avg}")

# Find the best number of clusters based on the highest silhouette score
best_n_clusters = max(silhouette_scores, key=lambda x: x[1])[0]
print(f"Best number of clusters: {best_n_clusters}")

# Plot Hierarchical

hierarchical_cluster = AgglomerativeClustering(n_clusters= best_n_clusters, metric="euclidean",linkage="ward")
hierarchical_labels = hierarchical_cluster.fit(cluster_data)
labels_hierarchical = hierarchical_labels.labels_

# Add labels to dataframe
embedding.loc[:,"hierarchical_labels"] = labels_hierarchical
u_labels = np.unique(labels_hierarchical)
 
# Plot results
for i, label in enumerate(u_labels):
    color = cmap((i)/len(u_labels))
    plt.scatter(cluster_data.iloc[labels_hierarchical == label , 0] , 
                cluster_data.iloc[labels_hierarchical == label , 1] , 
                label = label, color = color )
plt.legend(loc="center left", bbox_to_anchor=(1, 0.5),title="Clusters")
plt.title("Hierarchical")
plt.xlabel("umap 1")
plt.ylabel("umap 2")
plt.savefig("./Output/plot_hierarchical.svg",bbox_inches='tight')
plt.show()

# Dendrogram

# Hierarchical Clustering durchführen, um die Linkage-Matrix für das Dendrogramm zu erhalten
Z = linkage(cluster_data, method="ward", metric="euclidean")

# Dendrogramm erstellen
plt.figure(figsize=(15, 8))
dendrogram(Z, leaf_rotation=90., leaf_font_size=5, labels=embedding.loc[:,"USK"].tolist()) # tolist, weil sonst Warnung, weil dendogram eine Liste erwartet und kein dataframe
plt.xlabel("USK of Videos")
plt.ylabel("Clusterdistance")
plt.title("Dendrogramm Hierarchical Clustering")
plt.show()

# Scores for Hierarchical Clustering

sil_hierarchical = silhouette_score(cluster_data, labels_hierarchical)
davies_hierarchical = davies_bouldin = davies_bouldin_score(cluster_data, labels_hierarchical)
print("davies_bouldin_score", davies_hierarchical)
print("silhouette_score",sil_hierarchical)

# Scores for all clustering methods
scores_df = pd.DataFrame({'Score':["Silhouette","Davies-Bouldin"]})
scores_df.set_index('Score', inplace=True)
scores_df["K-Means"]= [sil_kmeans, davies_kmeans]
scores_df["DBSCAN"]= [sil_dbscan, davies_dbscan]
scores_df["Hierarchical"]= [sil_hierarchical, davies_hierarchical]
scores_df.to_csv("./Output/scores.csv")

scores_df_trans = scores_df.transpose()
scores_df_trans.to_csv("./Output/scores2.csv")

# # Distributed bar charts

# K-means

kmeans_plot = embedding.loc[:,["USK", "kmeans labels"]]
kmeans_plot

# Count USK per K-Means label
count_USK = kmeans_plot.groupby(["kmeans labels","USK"]).size().unstack(fill_value=0).stack().reset_index(name = "count")

df_kmeans = count_USK.pivot(index="kmeans labels", columns="USK", values='count')

labels_kmeans = df_kmeans.index.tolist()
N = len(labels_kmeans)
usk_list = count_USK["USK"].unique().tolist()

barWidth = .5
xloc = np.arange(N)

# gestapeltes Balkendiagramm anzeigen
for i in range(len(usk_list)):
    plt.bar(xloc, df_kmeans.iloc[:,i], bottom=df_kmeans.iloc[:,0:i].sum(axis=1),color = cmap((2*i+1)/20))
    
plt.legend(usk_list, loc="center left", bbox_to_anchor=(1, 0.5), title="USK")
plt.xlabel("Clusters")
plt.ylabel("Number of games")
plt.title("Distributed bar chart: K-Means")

plt.savefig("./Output/barchart_kmeans.svg",bbox_inches='tight')
plt.show()

# DBSCAN

DBSCAN_plot = embedding.loc[:,["USK", "DBSCAN_labels"]]
count_USK = DBSCAN_plot.groupby(["DBSCAN_labels","USK"]).size().unstack(fill_value=0).stack().reset_index(name = "count").copy()

#remove -1 label
df_dbscan = count_USK.pivot(index="DBSCAN_labels", columns="USK", values='count').iloc[1:,:]
df_dbscan

labels_dbscan = df_dbscan.index.tolist()
N = len(labels_dbscan)

barWidth = .5
xloc = labels_dbscan

# gestapeltes Balkendiagramm anzeigen
for i in range(len(usk_list)):
    plt.bar(xloc, df_dbscan.iloc[:,i], bottom=df_dbscan.iloc[:,0:i].sum(axis=1),color = cmap((2*i+1)/20))
plt.legend(usk_list, loc="center left", bbox_to_anchor=(1, 0.5), title="USK")
plt.xlabel("Clusters")
plt.ylabel("Number of games")
plt.title("Distributed bar chart: DBSCAN")
plt.savefig("./Output/barchart_dbscan.svg",bbox_inches='tight')
plt.show()

# Hierarchical

hierarchical_plot = embedding.loc[:,["USK", "hierarchical_labels"]]
count_USK= hierarchical_plot.groupby(["hierarchical_labels","USK"]).size().unstack(fill_value=0).stack().reset_index(name = "count").copy()
df_hierarchical = count_USK.pivot(index="hierarchical_labels", columns="USK", values='count')
df_hierarchical

labels_hierarchical = df_hierarchical.index.tolist()

N = len(labels_hierarchical)

barWidth = .5
xloc = labels_hierarchical

# gestapeltes Balkendiagramm anzeigen
for i in range(len(usk_list)):
    plt.bar(xloc, df_hierarchical.iloc[:,i], bottom=df_hierarchical.iloc[:,0:i].sum(axis=1),color = cmap((2*i+1)/20))
plt.legend(usk_list, loc="center left", bbox_to_anchor=(1, 0.5),title="USK")
plt.xlabel("Clusters")
plt.ylabel("Number of games")
plt.title("Distributed bar chart: Hierarchical")
plt.savefig("./Output/barchart_hierarchical.svg",bbox_inches='tight')
plt.show()
