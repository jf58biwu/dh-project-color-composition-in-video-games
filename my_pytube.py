from pytube import YouTube
import os

# Spezifizierung, wo Videos gespeichert werden sollen 
result_folder = r".\Videos"
if not os.path.exists(result_folder):
    os.makedirs(result_folder)

# Laden der URL's
with open("game_urls.txt") as file: 
    lines = [line.rstrip() for line in file]
    print(lines)

# Download aller Videos 
for link in lines: 
    YouTube(link, use_oauth=True, allow_oauth_cache=True).streams.get_highest_resolution().download(result_folder)

print("Bääähm hat funktioniert")
