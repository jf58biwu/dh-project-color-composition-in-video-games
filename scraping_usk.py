import requests, csv
from bs4 import BeautifulSoup

filename = "gamelist.csv"
with open(filename,"w") as file:
    writer = csv.writer(file, delimiter=",")
    writer.writerow(["Game","Age"])

ages = [(0,"freigegeben-ohne-altersbeschraenkung"),
        (6,"freigegeben-ab-6-jahren"),
        (12,"freigegeben-ab-12-jahren"),
         (16,"freigegeben-ab-16-jahren"),
         (18,"keine-jugendfreigabe")
]

genre="rollenspiel-actionorientiert"
#genre = "action-adventure"

def scrape_titles(paged, gamelist, rating):
    URL = f"https://usk.de/en/?paged={paged}&post_type=usktitle&genre={genre}&platform=pc&rating={rating}-gemaess-%C2%A7-14-juschg"
   
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")
    
    search_results = soup.find_all('div', class_="usktitle-card-game-list-title")
    if (len(search_results)) == 0:
        return gamelist
    for result in search_results:
        title = result.contents[0]
        print(title)
        gamelist.add(title)
       
    if len(gamelist) < 50:
        paged+=1
        scrape_titles(paged, gamelist, rating)
    
    return gamelist



for (age,rating) in ages:
    gamelist = set()
    print(age,rating)
    titles = scrape_titles(1, gamelist, rating)
    with open(filename,"a") as file:
        writer = csv.writer(file, delimiter=",")
        for title in titles:
            writer.writerow([title,age])
    print(len(gamelist))
    
    
    
