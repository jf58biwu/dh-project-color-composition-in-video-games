import os 
import cv2
import math
import glob

# Specify the output folder path
output_folder = r".\Extract_Output"

# Create the output folder if it doesn't exist
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

video_filename = glob.glob(r".\Videos\*.mp4")
for trailer in video_filename: 
    video_folder = os.path.join(output_folder, os.path.splitext(os.path.basename(trailer))[0])
    if not os.path.exists(video_folder):
        os.makedirs(video_folder)
    
    overwrite = True

    # Opens the Video file
    cap= cv2.VideoCapture(trailer)
    fps = cap.get(cv2.CAP_PROP_FPS) # Analyse how many frames per second
    print("fps", fps)
    start = int(fps * 5) # Hier nochmal schauen, ob und wann wir aufhören
    end = fps * 600

        # Adjust 'end' to be four seconds before the actual end of the video
    total_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    seconds_before_end = max(total_frames - fps * 5, 0)
    end = min(end, seconds_before_end)

    if start < 0:  # If start isn't specified lets assume 0
            start = 0
    if end > 0:  # If end isn't specified assume the end of the video
            end = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    i=0
    every = math.floor(fps) * 3 # Image every 3 seconds
    print(every)
    cap.set(1, start) 
    frame = start  # Keep track of which frame we are up to, starting from start
    while_safety = 0  # A safety counter to ensure we don't enter an infinite while loop (hopefully we won't need it)
    saved_count = 0  # A count of how many frames we have saved

    while frame < end:  # Lets loop through the frames until the end
            
            _, img = cap.read()  # Read an image from the capture   - _, zum unpacking

            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            
            thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY)[1]
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(15,15))
            morph = cv2.morphologyEx(thresh,cv2.MORPH_OPEN, kernel)
            contours,_ = cv2.findContours(morph,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            
            if while_safety > 500:  # Break the while if our safety maxs out at 500
                break

            # Sometimes OpenCV reads None's during a video, in which case we want to just skip
            if img is None:  # If we get a bad return flag or the image we read is None, lets not save
                while_safety += 1  # Add 1 to our while safety, since we skip before incrementing our frame variable
                continue  # Skip
            min_contour_area = 100  # Adjust this value as per your requirement
            filtered_contours = [con for con in contours if cv2.contourArea(con) > min_contour_area]

            if len(filtered_contours) > 0:  # Check if any contours are found
                big_contour = max(filtered_contours, key=cv2.contourArea)  # find the maximum contour , key=cv2.contourArea
                x, y, w, h = cv2.boundingRect(big_contour)

                crop = img[y:y+h, x:x+w]

                if frame % every == 0:  # If this is a frame we want to write out based on the 'every' argument
                    while_safety = 0  # Reset the safety count
                    print(f"generating frame:{frame}/{end}")

                    image_path = os.path.join(video_folder, "{:010d}.jpg".format(frame))
                    cv2.imwrite(image_path, crop)  # Save the extracted image
                    saved_count += 1  # Increment our counter by one   
        
            frame += 1  # Increment our frame count

    cap.release()  # After the while has finished close the capture


print(saved_count)  # Return the count of the images we saved

cv2.destroyAllWindows()