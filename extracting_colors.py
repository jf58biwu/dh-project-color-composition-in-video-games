import csv
import glob, os
import csv
import extcolors

from PIL import Image 
from os.path import exists


# Generieren hier neue Ordner
output_folder = glob.glob(r".\Extract_Output\*")
no_list_output_folder = r".\Extract_Output" # wird benötigt, da glob alle gefundenen Daten in einer Liste speichert
output_width = 900

# Zugreifen auf Ordner, um an die Bilder zu kommen
for folder in output_folder:
    folder_name = os.path.basename(folder)
    for index, infile in enumerate(os.listdir(folder)): 
        file, file_extension = os.path.splitext(infile)
        old_image_path = os.path.join(folder, infile)
        # Bilder werden geöffnet und resized, daraufhin wird jedes Bild nach den erst 4 Wörtern eines Videospielordners benannt und gespeichert
        with Image.open(old_image_path) as im:
            wide_percent = (output_width/float(im.size[0]))
            height_size = int((float(im.size[1])*float(wide_percent)))
            resize_image = im.resize((output_width,height_size), Image.LANCZOS)
            name_parts = folder_name.split()
            shorter_filename = '_'.join(name_parts[:4]) + f"_{index:01d}{file_extension}"
            
            new_image_path = os.path.join(folder, shorter_filename)
            resize_image.save(new_image_path)
            
            # Lösche das alte Bild
            if os.path.exists(old_image_path):
                os.remove(old_image_path)

def write_color_information_to_csv(output_folder, csv_filename):
    # Öffne die CSV-Datei zum Schreiben oder erstelle sie, falls sie nicht existiert
    csv_path = os.path.join(output_folder, csv_filename)
    with open(csv_path, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)

        # Schreibe die Kopfzeile mit den Spaltennamen
        csv_writer.writerow(["Filename", "Ext_Colors_1", "Ext_Colors_2", "Ext_Colors_3", "Ext_Colors_4", "Ext_Colors_5"])

        # Durchlaufe alle Bilder in den Ordnern
        for folder in sorted(os.listdir(output_folder)):
            folder_path = os.path.join(output_folder, folder)
            if os.path.isdir(folder_path):
                files = sorted(os.listdir(folder_path), key=lambda x: (x.split("_")[-1].split(".")[0].zfill(5)))
                for filename in files:

                    image_path = os.path.join(folder_path, filename)

                    # Extrahiere Farbinformationen für das aktuelle Bild
                    extcolors_result = extcolors.extract_from_path(image_path, tolerance=3, limit=5)
                    colors_info = extcolors_result[0]  # Liste von Farben und ihrem Vorkommen
                    
                    # Füge fehlende Farben hinzu, falls weniger als 5 Farben extrahiert wurden
                    while len(colors_info) < 5:
                        colors_info.append((0,0)) # Hier wurde händisch aus None "(0,0)", da es in einem späteren Part des Codes zu einem Konflikt kam (Es stand hier zuvor ((None,None)))
                        
                    # Formatiere die Farbinformationen mit einem Semikolon als Trennzeichen
                    formatted_colors = ";".join([f"({", ".join(map(str, color[:-1]))}, {color[-1]})" for color in colors_info])

                    # Schreibe den Dateinamen und die extrahierten Farben in die CSV-Datei
                    csv_row = [filename, formatted_colors]
                    print(csv_row)
                    csv_writer.writerow(csv_row)

    print("yes, girl it worked!", csv_path, "saaaaave")

# Aufruf der Funktion
csv_filename = "color_information.csv"
write_color_information_to_csv(no_list_output_folder, csv_filename)




