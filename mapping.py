import numpy as np
import re
import pandas as pd
import csv
import matplotlib.pyplot as plt

from os.path import exists
from colormath import color_diff_matrix
from colormath.color_objects import LabColor,sRGBColor
from colormath.color_conversions import convert_color


# Filtern der RGB Werte, sowie deren Vorkommen, um vorerst mit den RGB Werten eine weitere Anlyse stattfinden zu lassen
extracted_rgb_values = []
extracted_occurence =[]

def extract_occurrences(rgb_string):
        return [tuple(map(int, rgb.replace(" ", "").replace(')', '').strip('()').split(',')))[3] for rgb in rgb_string.split(';')]

def extract_rgb_value(rgb_string):
        return [tuple(map(int, rgb.replace(" ", "").replace(')', '').strip('()').split(',')))[:-1] for rgb in rgb_string.split(';')]
with open(r".\Extract_Output\color_information_all.csv", mode = "r", encoding="utf-8") as file:
    csvFile = csv.reader(file)
    next(csvFile)
    for row in csvFile:
        imagename = row[0]
        if "(0, 0)" in row[1]:
            continue
        rgb_values = extract_rgb_value(row[1])
        occurrence = extract_occurrences(row[1])
        extracted_rgb_values.append((imagename, rgb_values))
        extracted_occurence.append((imagename, occurrence))


# Funktion, um Palette zu generieren, die als Mappinggrundlage unserer Matrix dient 
def create_palette_and_names():

## Mit Unbuntfarben  
    # colors = [(255,255,0), (128,255,0), (0,255,0), (0,255,128),
    #           (0,255,255), (0,128,255), (0,0,255), (128,0,255), 
    #           (255,0,255), (255,0,128), (255,0,0), (255,128,0), 
    #           (255,255,255), (192,192,192), (128,128,128), (64,64,64), (0,0,0)]
    # names = [
    #     "Gelb", "Gelbgruen", "Gruen", "Blaugruen", "Cyanblau", "Blau", "Violett", 
    #     "Rotviolett", "Magentarot", "Rot", "Orange", "Gelborange", "Weiss", "Hellgrau", 
    #     "Grau", "Dunkelgrau", "Schwarz" 
    # ]

## Ohne Unbuntfarben 
    colors = [(255,255,0), (128,255,0), (0,255,0), (0,255,128),
              (0,255,255), (0,128,255), (0,0,255), (128,0,255), 
              (255,0,255), (255,0,128), (255,0,0), (255,128,0), 
              ]
    names = [
        "Gelb", "Gelbgruen", "Gruen", "Blaugruen", "Cyanblau", "Blau", "Violett", 
        "Rotviolett", "Magentarot", "Rot", "Orange", "Gelborange"
    ]


    palette_with_name = [(name, color) for name, color in zip(names,colors)]
    return palette_with_name

palette = create_palette_and_names()


# Funktion, um die ausgewählten Farben von Küppers plotten zu können 
def show_colors(color_list):
    
    num_colors = len(color_list)
    rows = (num_colors // 5) + 1  # Berechnung der Anzahl der Reihen anhand der Anzahl der Farben
    fig, ax = plt.subplots(nrows=rows, ncols=5, figsize=(10, rows*2), subplot_kw={'xticks': [], 'yticks': []})
    
    # Iteration über die color_list, um daraufhin subplots für jede Farbe zu generieren
    for i, (name, color) in enumerate(color_list):

        row, col = i // 5, i % 5  # Calculate the row and column indices
        ax[row, col].imshow([[color]])  # Display the color as an image
        ax[row, col].set_title(f"{name}\nRGB:{color}", fontsize=8)  # Set the title
    
    #Entfernen von leeren Subplots, wenn die Anzahl der Farben nicht durch 5 teilabr ist
    for i in range(num_colors, rows*5):
        row, col = i // 5, i % 5
        fig.delaxes(ax[row, col])

    plt.tight_layout()  
    plt.show()

# Show the colors
show_colors(palette)


## Umschreiben der delta_e_cie2000 Funktion 
def _get_lab_color1_vector(color):

    if not color.__class__.__name__ == "LabColor":
        raise ValueError(
            "Delta E functions can only be used with two LabColor objects."
        )
    return np.array([color.lab_l, color.lab_a, color.lab_b])


def _get_lab_color2_matrix(color):

    if not color.__class__.__name__ == "LabColor":
        raise ValueError(
            "Delta E functions can only be used with two LabColor objects."
        )
    return np.array([(color.lab_l, color.lab_a, color.lab_b)])

def delta_e_cie2000(color1, color2, Kl=1, Kc=1, Kh=1):
 
    color1_vector = _get_lab_color1_vector(color1)
    color2_matrix = _get_lab_color2_matrix(color2)
    delta_e = color_diff_matrix.delta_e_cie2000(
        color1_vector, color2_matrix, Kl=Kl, Kc=Kc, Kh=Kh)[0]
    return delta_e.item() # Ändern von asscalar() zu item() wegen einem NumPy Fehler


#Funktion, um mithilfe von delta_e_cie2000 die Distanz der Farben in Bezug auf andere Farben zu berechnen
def find_closest_color(color, palette_colors):
    min_distance = float('inf') # Initialisiere die minimale Distanz mit einem sehr hohen Wert
    closest_color = None # Initialisiere die Variable für die am nächsten liegende Farbe mit None
    closest_index = -1 # Initialisiere den Index der am nächsten liegenden Farbe mit -1

    # Konvertiere die Eingabefarbe in LabColor
    color_rgb = sRGBColor(*color) # Konvertiere RGB-Wert in sRGBColor
    color_lab = convert_color(color_rgb, LabColor) # Konvertiere sRGBColor in LabColor
    
    # Iteriere durch die Küppers Farben und finde die am nächsten liegende Farbe zur Eingabefarbe
    for index, palette_color in enumerate(palette_colors):
        palette_color_rgb = sRGBColor(*palette_color) # Konvertiere RGB-Wert in sRGBColor
        palette_color_lab = convert_color(palette_color_rgb, LabColor) # Konvertiere sRGBColor in LabColor
        distance = delta_e_cie2000(color_lab, palette_color_lab) # Berechne den Unterschied 

        # Überprüfe, ob die aktuelle Farbe näher an der Eingabefarbe liegt als die vorherige am nächsten liegende Farbe
        if distance < min_distance:
            min_distance = distance # Aktualisiere minimale Distanz
            closest_color = palette_color # Aktualisiere am nächsten liegende Farbe 
            closest_index = index # Aktualisiere Index der am nächsten liegenden Farbe 

    return closest_color, closest_index

#Funktion, zur Erstellung der Matrix
def create_color_matrix(image_colors, palette_colors):
    color_matrix = [] # leere Matrix

    # Iteriere über jede Liste von Farben in den Bildern
    for image_name, image in image_colors:
        image_matrix = [0] * len(palette_colors)  # Matrix mit Nullen initialisieren

        # Iteriere über jede Farbe in einem Bild
        for color in image:
            closest_color, closest_index = find_closest_color(color, palette_colors) # Finde die am nächsten gelegenen Webfarben für die Farben des Bildes
            if closest_index != -1:  # Überprüfe, ob eine am nächsten gelegene Farbe gefunden wurde
                image_matrix[closest_index] += 1  # Setze den Index der am nächsten gelegenen Farbe auf +1

        color_matrix.append(image_matrix)

    return color_matrix
         

# Erstellen eines Dictionary, das den Farbnamen zu jedem Index in der Palette zuordnet
color_names = {index: colorname for index, (colorname, _) in enumerate(palette)}

# Erstellen einer Farbmatrix
color_matrix = create_color_matrix(extracted_rgb_values, [color for name, color in palette])

# Konvertiere die Farbmatrix in ein pandas DataFrame
df = pd.DataFrame(color_matrix, columns=[color_names[i] for i in range(len(palette))], index=[name for name, _ in extracted_rgb_values])

# Funktion, um bei den Indexnamen besagtes Pattern zu entfernen -> für darauffolgendes groupby anhand vom Index
def skip_jpg(text):
    pattern = r"_\d+\.jpg"
    result = re.sub(pattern, "",text)
    return result

df.index = df.index.map(skip_jpg)

# Gruppierung durch Index, um mithilfe des Durchschnittes zu normalisieren
grouped_df = df.groupby(df.index).mean()

# Abspeichern der csv
grouped_df.to_csv(r".\Extract_Output\mapped_Matrix_addition_unbunt.csv")